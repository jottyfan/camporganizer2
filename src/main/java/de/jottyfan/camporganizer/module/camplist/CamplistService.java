package de.jottyfan.camporganizer.module.camplist;

import static de.jottyfan.camporganizer.db.jooq.Tables.V_CAMP;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.jooq.Condition;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.db.jooq.tables.records.VCampRecord;
import de.jottyfan.camporganizer.module.camplist.model.BookingBean;
import de.jottyfan.camporganizer.module.dashboard.DashboardRepository;

/**
 *
 * @author jotty
 *
 */
@Service
public class CamplistService {

	@Autowired
	private CamplistGateway gateway;

	@Autowired
	private DashboardRepository dashboardGateway;

	/**
	 * get all camps from the database and prepare them for the view
	 *
	 * @return the list of found camps
	 */
	public List<VCampRecord> getAllCamps(Boolean upcomingOnly) {
		Condition condition = upcomingOnly ? V_CAMP.DEPART.greaterOrEqual(LocalDateTime.now()) : DSL.trueCondition();
		Stream<VCampRecord> stream = gateway.getAllCamps(condition);
		List<VCampRecord> list = new ArrayList<>();
		stream.forEach(o -> list.add(o));
		return list;
	}

	/**
	 * get all bookings that the user can see
	 *
	 * @param username the name of the user
	 * @return a list of beans
	 */
	public List<BookingBean> getBookingsOf(String username) {
		return gateway.getAllBookings(username);
	}

	/**
	 * update defined contents of the bean
	 *
	 * @param bean the bean
	 * @return true or false
	 */
	public Boolean update(BookingBean bean) {
		return dashboardGateway.update(bean) == 1;
	}
}
