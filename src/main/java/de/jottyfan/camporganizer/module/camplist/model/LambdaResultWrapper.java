package de.jottyfan.camporganizer.module.camplist.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author henkej
 *
 */
public class LambdaResultWrapper {
	private Integer counter;
	private Map<String, Boolean> mapBoolean;
	private Map<String, String> mapString;

	public LambdaResultWrapper() {
		this.mapBoolean = new HashMap<>();
		this.mapString = new HashMap<>();
		counter = 0;
	}

	public Integer getCounter() {
		return counter;
	}

	public void add(Integer i) {
		counter += i;
	}

	public void putBoolean(String key, Boolean value) {
		mapBoolean.put(key, value);
	}

	public Boolean getBoolean(String key) {
		return mapBoolean.get(key);
	}

	public void putString(String key, String value) {
		mapString.put(key, value);
	}

	public String getString(String key) {
		return mapString.get(key);
	}
}
