package de.jottyfan.camporganizer.module.camplist.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class DocumentBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}
}
