package de.jottyfan.camporganizer.module.camplist.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;

/**
 *
 * @author jotty
 *
 */
public class BookingBean implements Serializable {
	private static final long serialVersionUID = 4L;

	private Integer pk;
	private String forename;
	private String surname;
	private String street;
	private String zip;
	private String city;
	private String phone;
	private LocalDate birthdate;
	private EnumCamprole camprole;
	private String comment;
	private Boolean consentCatalogPhoto;
	private LocalDateTime created;
	private String email;
	private EnumSex sex;
	private String locationName;
	private String price;
	private Double year;
	private LocalDateTime arrive;
	private LocalDateTime depart;
	private String countries;
	private String url;
	private Boolean isOver;
	private String campName;
	private String registrator;
	private Boolean accept;
	private String subscriber;

	private List<DocumentBean> documents;

	public BookingBean() {
		this.documents = new ArrayList<>();
	}

	public Boolean isFemale() {
		return EnumSex.female.equals(sex);
	}

	public Boolean isMale() {
		return EnumSex.male.equals(sex);
	}

	public Boolean isTeacher() {
		return EnumCamprole.teacher.equals(camprole);
	}

	public Boolean isStudent() {
		return EnumCamprole.student.equals(camprole);
	}

	public Boolean isDirector() {
		return EnumCamprole.director.equals(camprole);
	}

	public Boolean isFeeder() {
		return EnumCamprole.feeder.equals(camprole);
	}

	public Boolean isObserver() {
		return EnumCamprole.observer.equals(camprole);
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the birthdate
	 */
	public LocalDate getBirthdate() {
		return birthdate;
	}

	/**
	 * @param birthdate the birthdate to set
	 */
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * @return the camprole
	 */
	public EnumCamprole getCamprole() {
		return camprole;
	}

	/**
	 * @param camprole the camprole to set
	 */
	public void setCamprole(EnumCamprole camprole) {
		this.camprole = camprole;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the consentCatalogPhoto
	 */
	public Boolean getConsentCatalogPhoto() {
		return consentCatalogPhoto;
	}

	/**
	 * @param consentCatalogPhoto the consentCatalogPhoto to set
	 */
	public void setConsentCatalogPhoto(Boolean consentCatalogPhoto) {
		this.consentCatalogPhoto = consentCatalogPhoto;
	}

	/**
	 * @return the created
	 */
	public LocalDateTime getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the sex
	 */
	public EnumSex getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(EnumSex sex) {
		this.sex = sex;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the location_name to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the year
	 */
	public Double getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Double year) {
		this.year = year;
	}

	/**
	 * @return the arrive
	 */
	public LocalDateTime getArrive() {
		return arrive;
	}

	/**
	 * @param arrive the arrive to set
	 */
	public void setArrive(LocalDateTime arrive) {
		this.arrive = arrive;
	}

	/**
	 * @return the depart
	 */
	public LocalDateTime getDepart() {
		return depart;
	}

	/**
	 * @param depart the depart to set
	 */
	public void setDepart(LocalDateTime depart) {
		this.depart = depart;
	}

	/**
	 * @return the countries
	 */
	public String getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public void setCountries(String countries) {
		this.countries = countries;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the is_over
	 */
	public Boolean getIsOver() {
		return isOver;
	}

	/**
	 * @param isOver the isOver to set
	 */
	public void setIsOver(Boolean isOver) {
		this.isOver = isOver;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}

	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public String getRegistrator() {
		return registrator;
	}

	public void setRegistrator(String registrator) {
		this.registrator = registrator;
	}

	public Boolean getAccept() {
		return accept;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept;
	}

	public String getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}

	public List<DocumentBean> getDocuments() {
		return documents;
	}

}
