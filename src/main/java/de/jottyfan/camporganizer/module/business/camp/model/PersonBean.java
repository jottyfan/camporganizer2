package de.jottyfan.camporganizer.module.business.camp.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class PersonBean implements Serializable {
	private static final long serialVersionUID = 2L;

	private Integer pk;
	private String name;
	private String role;
	private String sex;
	private Boolean accept;
	private LocalDateTime bookingDate;
	private BigDecimal paid;

	/**
	 * @return the accept
	 */
	public Boolean getAccept() {
		return accept;
	}

	/**
	 * @param accept the accept to set
	 */
	public void setAccept(Boolean accept) {
		this.accept = accept;
	}

	/**
	 * @return the paid
	 */
	public BigDecimal getPaid() {
		return paid;
	}

	/**
	 * @param paid the paid to set
	 */
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}
}
