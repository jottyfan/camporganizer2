package de.jottyfan.camporganizer.module.business.bookings;

import jakarta.annotation.security.RolesAllowed;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.jottyfan.camporganizer.module.business.bookings.model.AddPaymentBean;
import de.jottyfan.camporganizer.module.business.bookings.model.BookerBean;
import de.jottyfan.camporganizer.module.camplist.CommonController;

/**
*
* @author jotty
*
*/
@Controller
public class BookingsController extends CommonController {
	private static final Logger LOGGER = LogManager.getLogger(BookingsController.class);

	@Autowired
	private BookingsService bookingsService;

	@GetMapping("/business/bookings")
	@RolesAllowed({"business_booking"})
	public String getBookings(Model model) {
		model.addAttribute("bookers", bookingsService.getBookers(getCurrentUser()));
		model.addAttribute("addBean", new AddPaymentBean());
		return "business/bookings";
	}

	@GetMapping("/business/bookings/{id}")
	@RolesAllowed({"business_booking"})
	public String getBooking(Model model, @PathVariable Integer id) {
		BookerBean bean = bookingsService.getBooker(id, getCurrentUser());
		model.addAttribute("booker", bean);
		model.addAttribute("addBean", new AddPaymentBean());
		return bean == null ? getBookings(model) : "business/booker";
	}

	@PostMapping("/business/bookings/payment/{id}")
	@RolesAllowed({"business_booking"})
	public String addPayment(Model model, @ModelAttribute AddPaymentBean bean, @PathVariable Integer id) {
		Double payment = bean.getPayment();
		bookingsService.addPayment(id, payment);
		return getBooking(model, id);
	}

	@PostMapping("/business/bookings/listpayment/{id}")
	@RolesAllowed({"business_booking"})
	public String addListPayment(Model model, @ModelAttribute AddPaymentBean bean, @PathVariable Integer id, @RequestParam(defaultValue = "") String search) {
		Double payment = bean.getPayment();
		bookingsService.addPayment(id, payment);
		LOGGER.debug("search is {}", search);
		model.addAttribute("search", search);
		return getBookings(model);
	}
}
