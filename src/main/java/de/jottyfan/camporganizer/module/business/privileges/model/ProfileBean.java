package de.jottyfan.camporganizer.module.business.privileges.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author jotty
 *
 */
public class ProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String forename;
	private String surname;
	private String username;
	private LocalDateTime duedate;

	public String getFullname() {
		return new StringBuilder().append(forename).append(" ").append(surname).toString();
	}

	/**
	 * dropdown representation
	 *
	 * @return the dropdown string
	 */
	public String dropdown() {
		StringBuilder buf = new StringBuilder();
		buf.append(forename).append(" ");
		buf.append(surname).append(" (");
		buf.append(username).append(", ");
		buf.append(duedate == null ? "" : duedate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		buf.append(")");
		return buf.toString();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("{pk=").append(pk);
		buf.append(", forename=").append(forename);
		buf.append(", surname=").append(surname);
		buf.append(", username=").append(username);
		buf.append(", duedate=").append(duedate);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the duedate
	 */
	public LocalDateTime getDuedate() {
		return duedate;
	}

	/**
	 * @param duedate the duedate to set
	 */
	public void setDuedate(LocalDateTime duedate) {
		this.duedate = duedate;
	}

}
