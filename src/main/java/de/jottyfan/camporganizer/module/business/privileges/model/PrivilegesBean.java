package de.jottyfan.camporganizer.module.business.privileges.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class PrivilegesBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String forename;
	private String surname;
	private String username;
	private LocalDateTime duedate;
	private Integer fkProfile;
	private Integer fkCamp;
	private String campName;
	private Double campYear;

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the fkProfile
	 */
	public Integer getFkProfile() {
		return fkProfile;
	}

	/**
	 * @param fkProfile the fkProfile to set
	 */
	public void setFkProfile(Integer fkProfile) {
		this.fkProfile = fkProfile;
	}

	/**
	 * @return the fkCamp
	 */
	public Integer getFkCamp() {
		return fkCamp;
	}

	/**
	 * @param fkCamp the fkCamp to set
	 */
	public void setFkCamp(Integer fkCamp) {
		this.fkCamp = fkCamp;
	}

	/**
	 * @return the campName
	 */
	public String getCampName() {
		return campName;
	}

	/**
	 * @param campName the campName to set
	 */
	public void setCampName(String campName) {
		this.campName = campName;
	}

	/**
	 * @return the campYear
	 */
	public Double getCampYear() {
		return campYear;
	}

	/**
	 * @param campYear the campYear to set
	 */
	public void setCampYear(Double campYear) {
		this.campYear = campYear;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the duedate
	 */
	public LocalDateTime getDuedate() {
		return duedate;
	}

	/**
	 * @param duedate the duedate to set
	 */
	public void setDuedate(LocalDateTime duedate) {
		this.duedate = duedate;
	}
}
