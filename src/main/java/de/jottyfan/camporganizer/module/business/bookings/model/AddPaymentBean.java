package de.jottyfan.camporganizer.module.business.bookings.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class AddPaymentBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Double payment;

	public Double getPayment() {
		return payment;
	}

	public void setPayment(Double payment) {
		this.payment = payment;
	}
}
