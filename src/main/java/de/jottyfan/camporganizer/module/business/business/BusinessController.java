package de.jottyfan.camporganizer.module.business.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.camporganizer.module.camplist.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class BusinessController extends CommonController {

	@Autowired
	private BusinessService service;

	@GetMapping("/business")
	public String getIndex(Model model) {
		String username = super.getCurrentUser();
		model.addAttribute("campBudgets", service.getCampBudgets(username));
		return "business/business";
	}
}
