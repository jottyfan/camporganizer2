package de.jottyfan.camporganizer.module.business.privileges;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.module.business.privileges.model.PrivilegesBean;
import de.jottyfan.camporganizer.module.camplist.CommonController;
import jakarta.annotation.security.RolesAllowed;
import jakarta.websocket.server.PathParam;

/**
 *
 * @author jotty
 *
 */
@Controller
public class PrivilegesController extends CommonController {

	@Autowired
	private PrivilegesService privilegesService;

	@GetMapping("/business/privileges")
	@RolesAllowed({ "admin" })
	public String getIndex(Model model) {
		String username = super.getCurrentUser();
		model.addAttribute("currentUser", username);
		model.addAttribute("privileges", privilegesService.getPrivileges());
		model.addAttribute("profiles", privilegesService.getProfiles(EnumCamprole.director, EnumCamprole.teacher));
		model.addAttribute("bean", new PrivilegesBean());
		return "business/privileges";
	}

	@PostMapping("/business/privileges/add")
	@RolesAllowed({ "admin" })
	public String getAdd(@ModelAttribute PrivilegesBean bean, Model model) {
		privilegesService.add(bean);
		return getIndex(model);
	}

	@GetMapping("/business/privileges/delete")
	@RolesAllowed({ "admin" })
	public String getDelete(@PathParam(value = "fkCamp") Integer fkCamp,
			@PathParam(value = "fkProfile") Integer fkProfile, Model model) {
		PrivilegesBean bean = new PrivilegesBean();
		bean.setFkCamp(fkCamp);
		bean.setFkProfile(fkProfile);
		privilegesService.remove(bean, super.getCurrentUser());
		return getIndex(model);
	}
}
