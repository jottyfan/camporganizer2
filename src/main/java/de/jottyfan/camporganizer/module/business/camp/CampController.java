package de.jottyfan.camporganizer.module.business.camp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.jottyfan.camporganizer.module.camplist.CommonController;
import jakarta.annotation.security.RolesAllowed;

/**
 *
 * @author jotty
 *
 */
@Controller
public class CampController extends CommonController {

	@Autowired
	private CampService campService;

	@GetMapping("/business/camp/{id}")
	@RolesAllowed({ "business" })
	public String getCamp(Model model, @PathVariable Integer id) {
		String username = super.getCurrentUser();
		model.addAttribute("currentUser", username);
		model.addAttribute("campId", id);
		model.addAttribute("camp", campService.getCamp(id, username));
		model.addAttribute("booking", campService.getBookings(id, username));
		model.addAttribute("bookers", campService.getBookers(id, username));
		return "business/camp";
	}
}
