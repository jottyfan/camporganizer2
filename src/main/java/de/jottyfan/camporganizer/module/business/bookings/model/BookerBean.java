package de.jottyfan.camporganizer.module.business.bookings.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class BookerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;
	private String role;
	private String sex;
	private Boolean accept;
	private LocalDateTime bookingDate;
	private BigDecimal paid;
	private String camp;
	private String price;
	private Integer campId;
	private BigDecimal requiredPrice;

	/**
	 * @return the accept
	 */
	public Boolean getAccept() {
		return accept;
	}

	/**
	 * @param accept the accept to set
	 */
	public void setAccept(Boolean accept) {
		this.accept = accept;
	}

	/**
	 * @return the paid
	 */
	public BigDecimal getPaid() {
		return paid;
	}

	/**
	 * @param paid the paid to set
	 */
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getCamp() {
		return camp;
	}

	public void setCamp(String camp) {
		this.camp = camp;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getCampId() {
		return campId;
	}

	public void setCampId(Integer campId) {
		this.campId = campId;
	}

	/**
	 * @return the requiredPrice
	 */
	public BigDecimal getRequiredPrice() {
		return requiredPrice;
	}

	/**
	 * @param requiredPrice the requiredPrice to set
	 */
	public void setRequiredPrice(BigDecimal requiredPrice) {
		this.requiredPrice = requiredPrice;
	}
}
