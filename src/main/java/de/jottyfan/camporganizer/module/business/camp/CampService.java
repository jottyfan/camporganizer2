package de.jottyfan.camporganizer.module.business.camp;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.module.business.camp.model.BookingBean;
import de.jottyfan.camporganizer.module.business.camp.model.CampBean;
import de.jottyfan.camporganizer.module.business.camp.model.PersonBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class CampService {

	@Autowired
	private CampRepository campGateway;

	public CampBean getCamp(Integer id, String username) {
		return campGateway.getCamp(id, username);
	}

	public BookingBean getBookings(Integer id, String username) {
		Integer approved = 0;
		Integer open = 0;
		Integer rejected = 0;
		BigDecimal paid = new BigDecimal(0);
		for (PersonBean p : campGateway.getBookings(id, username)) {
			Boolean acceptence = p.getAccept();
			if (acceptence == null) {
				open += 1;
			} else if (acceptence) {
				approved += 1;
			} else {
				rejected += 1;
			}
			paid = paid.add(p.getPaid() == null ? new BigDecimal(0) : p.getPaid());
		}
		BookingBean bean = new BookingBean();
		bean.setApproved(approved);
		bean.setOpen(open);
		bean.setRejected(rejected);
		bean.setPaid(paid);
		return bean;
	}

	public List<PersonBean> getBookers(Integer id, String username) {
		return campGateway.getBookings(id, username);
	}
}
