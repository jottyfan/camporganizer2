package de.jottyfan.camporganizer.module.business.business.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class BusinessBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer fkPerson;
	private final String forename;
	private final String surname;
	private final String campName;
	private final String campPrice;
	private final LocalDateTime campArrive;
	private final String locationName;
	private BigDecimal paid;
	private boolean changed;
	private BigDecimal requiredPrice;

	public BusinessBean(Integer fkPerson, String forename, String surname, String campName, String campPrice,
			LocalDateTime campArrive, String locationName, BigDecimal paid, BigDecimal requiredPrice) {
		super();
		this.fkPerson = fkPerson;
		this.forename = forename;
		this.surname = surname;
		this.campName = campName;
		this.campPrice = campPrice;
		this.campArrive = campArrive;
		this.locationName = locationName;
		this.paid = paid;
		this.changed = false;
		this.requiredPrice = requiredPrice;
	}

	/**
	 * @return the paid
	 */
	public BigDecimal getPaid() {
		return paid;
	}

	/**
	 * @param paid the paid to set
	 */
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
		this.changed = true;
	}

	/**
	 * @return the fkPerson
	 */
	public Integer getFkPerson() {
		return fkPerson;
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @return the campName
	 */
	public String getCampName() {
		return campName;
	}

	/**
	 * @return the campPrice
	 */
	public String getCampPrice() {
		return campPrice;
	}

	/**
	 * @return the campArrive
	 */
	public LocalDateTime getCampArrive() {
		return campArrive;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	public boolean isChanged() {
		return changed;
	}

	/**
	 * @return the requiredPrice
	 */
	public BigDecimal getRequiredPrice() {
		return requiredPrice;
	}

	/**
	 * @param requiredPrice the requiredPrice to set
	 */
	public void setRequiredPrice(BigDecimal requiredPrice) {
		this.requiredPrice = requiredPrice;
	}
}
