package de.jottyfan.camporganizer.module.business.bookings;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.module.business.bookings.model.BookerBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class BookingsService {

	@Autowired
	private BookingsRepository bookingsGateway;

	public List<BookerBean> getBookers(String username) {
		return bookingsGateway.getBookings(username);
	}

	public BookerBean getBooker(Integer id, String username) {
		return bookingsGateway.getBooking(id, username);
	}

	public Integer addPayment(Integer id, Double payment) {
		return bookingsGateway.addPayment(id, payment);
	}
}
