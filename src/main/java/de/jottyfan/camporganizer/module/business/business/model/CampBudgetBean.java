package de.jottyfan.camporganizer.module.business.business.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author jotty
 *
 */
public class CampBudgetBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal budget;
	private String campName;
	private Double campYear;
	private Integer campId;

	public CampBudgetBean withCampId(Integer campId) {
		setCampId(campId);
		return this;
	}

	public CampBudgetBean withCampName(String campName) {
		setCampName(campName);
		return this;
	}

	public CampBudgetBean withCampYear(Double campYear) {
		setCampYear(campYear);
		return this;
	}

	public CampBudgetBean withBudget(BigDecimal budget) {
		setBudget(budget);
		return this;
	}

	/**
	 * @return the budget
	 */
	public BigDecimal getBudget() {
		return budget;
	}

	/**
	 * @param budget the budget to set
	 */
	public void setBudget(BigDecimal budget) {
		this.budget = budget;
	}

	/**
	 * @return the campName
	 */
	public String getCampName() {
		return campName;
	}

	/**
	 * @param campName the campName to set
	 */
	public void setCampName(String campName) {
		this.campName = campName;
	}

	/**
	 * @return the campYear
	 */
	public Double getCampYear() {
		return campYear;
	}

	/**
	 * @param campYear the campYear to set
	 */
	public void setCampYear(Double campYear) {
		this.campYear = campYear;
	}

	public Integer getCampId() {
		return campId;
	}

	public void setCampId(Integer campId) {
		this.campId = campId;
	}
}
