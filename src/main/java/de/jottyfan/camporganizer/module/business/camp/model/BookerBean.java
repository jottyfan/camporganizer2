package de.jottyfan.camporganizer.module.business.camp.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;

/**
 *
 * @author jotty
 *
 */
public class BookerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;
	private EnumCamprole campRole;
	private EnumSex sex;
	private LocalDateTime bookingDate;
	private BigDecimal paid;

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the campRole
	 */
	public EnumCamprole getCampRole() {
		return campRole;
	}

	/**
	 * @param campRole the campRole to set
	 */
	public void setCampRole(EnumCamprole campRole) {
		this.campRole = campRole;
	}

	/**
	 * @return the sex
	 */
	public EnumSex getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(EnumSex sex) {
		this.sex = sex;
	}

	/**
	 * @return the bookingDate
	 */
	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate the bookingDate to set
	 */
	public void setBookingDate(LocalDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @return the paid
	 */
	public BigDecimal getPaid() {
		return paid;
	}

	/**
	 * @param paid the paid to set
	 */
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}
}
