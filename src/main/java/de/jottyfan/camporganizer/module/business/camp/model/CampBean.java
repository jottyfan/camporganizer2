package de.jottyfan.camporganizer.module.business.camp.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import de.jottyfan.camporganizer.module.business.privileges.model.ProfileBean;

/**
 *
 * @author jotty
 *
 */
public class CampBean implements Serializable {
	private static final long serialVersionUID = 2L;

	private Integer pk;
	private String name;
	private Double year;
	private LocalDateTime arrive;
	private LocalDateTime depart;
	private String locationName;
	private String price;
	private List<ProfileBean> profiles;

	public CampBean() {
		profiles = new ArrayList<>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the year
	 */
	public Double getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Double year) {
		this.year = year;
	}

	/**
	 * @return the arrive
	 */
	public LocalDateTime getArrive() {
		return arrive;
	}

	/**
	 * @param arrive the arrive to set
	 */
	public void setArrive(LocalDateTime arrive) {
		this.arrive = arrive;
	}

	/**
	 * @return the depart
	 */
	public LocalDateTime getDepart() {
		return depart;
	}

	/**
	 * @param depart the depart to set
	 */
	public void setDepart(LocalDateTime depart) {
		this.depart = depart;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the profiles
	 */
	public List<ProfileBean> getProfiles() {
		return profiles;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}
}
