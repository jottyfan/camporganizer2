package de.jottyfan.camporganizer.module.business.camp.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author jotty
 *
 */
public class BookingBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer approved;
	private Integer rejected;
	private Integer open;
	private BigDecimal paid;

	/**
	 * @return the approved
	 */
	public Integer getApproved() {
		return approved;
	}

	/**
	 * @param approved the approved to set
	 */
	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	/**
	 * @return the rejected
	 */
	public Integer getRejected() {
		return rejected;
	}

	/**
	 * @param rejected the rejected to set
	 */
	public void setRejected(Integer rejected) {
		this.rejected = rejected;
	}

	/**
	 * @return the open
	 */
	public Integer getOpen() {
		return open;
	}

	/**
	 * @param open the open to set
	 */
	public void setOpen(Integer open) {
		this.open = open;
	}

	/**
	 * @return the paid
	 */
	public BigDecimal getPaid() {
		return paid;
	}

	/**
	 * @param paid the paid to set
	 */
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

}
