package de.jottyfan.camporganizer.module.business.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.module.business.business.model.CampBudgetBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class BusinessService {

	@Autowired
	private BusinessRepository gateway;

	public List<CampBudgetBean> getCampBudgets(String username) {
		return gateway.getCampBudgets(username);
	}
}
