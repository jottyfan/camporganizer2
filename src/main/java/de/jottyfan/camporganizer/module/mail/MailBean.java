package de.jottyfan.camporganizer.module.mail;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

/**
 *
 * @author jotty
 *
 */
public class MailBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty
	private final Set<String> to;

	@NotBlank
	private String from;
	private String subject;
	private String message;

	public MailBean() {
		this.to = new HashSet<>();
	}

	public String getTos() {
		StringBuilder buf = new StringBuilder();
		boolean first = true;
		for (String s : to) {
			buf.append(first ? "" : ";").append(s);
			first = false;
		}
		return buf.toString();
	}

	public void setTos(String tos) {
		this.to.clear();
		this.to.addAll(List.of(tos.split(";")));
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the to
	 */
	public Set<String> getTo() {
		return to;
	}
}
