package de.jottyfan.camporganizer.module.registration;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.jottyfan.camporganizer.db.EnumConverter;
import de.jottyfan.camporganizer.module.camplist.CommonController;
import de.jottyfan.camporganizer.module.registration.model.CampBean;
import de.jottyfan.camporganizer.module.registration.model.RegistrationBean;

/**
 *
 * @author jotty
 *
 */
@Controller
public class RegistrationController extends CommonController {

	@Autowired
	private RegistrationService service;

	@GetMapping("/registration/{fkCamp}")
	public String index(@PathVariable(name = "fkCamp", required = true) Integer fkCamp, Model model) {
		if (service.campIsNotYetOver(fkCamp)) {
			CampBean campBean = service.getCamp(fkCamp);
			model.addAttribute("camp", campBean);
			RegistrationBean bean = new RegistrationBean();
			bean.setFkCamp(fkCamp);
			bean.setRegisterInKeycloak(true); // we want people to register generally
			model.addAttribute("bean", bean);
			model.addAttribute("sexes", EnumConverter.getSexes());
			model.addAttribute("roles", EnumConverter.getRoles());
			return "/registration/registration";
		} else {
			return "/registration/isover";
		}
	}

	/**
	 * userlogin is protected and leads the request to the keycloak login mask if not yet logged in
	 *
	 * @param fkCamp the ID of the camp
	 * @param model the model
	 * @return hen registration page
	 */
	@GetMapping("/userlogin/registration/{fkCamp}")
	public String loginIndex(@PathVariable(name = "fkCamp", required = true) Integer fkCamp, Model model) {
		return index(fkCamp, model);
	}

	@PostMapping("/registration/register")
	public String register(@Valid @ModelAttribute("bean") RegistrationBean bean, final BindingResult bindingResult,
			Model model) {
		if (bindingResult.hasErrors()) {
			CampBean campBean = service.getCamp(bean.getFkCamp());
			model.addAttribute("camp", campBean);
			model.addAttribute("sexes", EnumConverter.getSexes());
			model.addAttribute("roles", EnumConverter.getRoles());
			return "/registration/registration";
		}
		Boolean result = service.register(bean, getCurrentUser());
		return result ? "/registration/success" : "/error";
	}

	@GetMapping("/registration/cancel/{id}")
	public String cancellation(@PathVariable Integer id, final Model model) {
		model.addAttribute("bean", service.getBooking(id));
		return "/registration/cancellation";
	}

	@GetMapping("/registration/remove/{id}")
	public String remove(@PathVariable Integer id, final Model model) {
		service.removeBooking(id);
		return "redirect:/dashboard";
	}

	@GetMapping("/registration/toggleconsent/{id}")
	public String toggleConsent(@PathVariable Integer id, final Model model) {
		service.toggleConsent(id);
		return "redirect:/dashboard";
	}
}
