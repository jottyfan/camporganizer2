package de.jottyfan.camporganizer.module.registration.validate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

/**
 *
 * @author jotty
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TeacherAgeCheckValidator.class)
@Documented
public @interface TeacherAgeCheck {
	String message() default "teacher is too young";

	String field();
	String fkCamp();
	String campRole();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
