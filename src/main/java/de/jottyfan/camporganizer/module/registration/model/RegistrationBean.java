package de.jottyfan.camporganizer.module.registration.model;

import java.io.Serializable;
import java.time.LocalDate;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;
import de.jottyfan.camporganizer.module.registration.validate.TeacherAgeCheck;
import de.jottyfan.camporganizer.module.registration.validate.UnusedUsername;

/**
 *
 * @author jotty
 *
 */
@UnusedUsername(field = "login", message = "Dieses Login ist leider bereits vergeben. Bitte wähle ein anderes.")
@TeacherAgeCheck(field = "birthDate", fkCamp = "fkCamp", campRole = "campRole", message = "Als Mitarbeiter bist Du leider zu jung für diese Freizeit.")
// TODO: registration completeness annotation; in case of registerInKeycloak == true, force login, password, kcForename, kcSurname and kcEmail not to be blank
public class RegistrationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Bitte gib deinen Vornamen an.")
	private String forename;
	@NotBlank(message = "Bitte gib deinen Nachnamen an.")
	private String surname;
	@NotNull(message = "Bitte gib dein Geschlecht an. Wir benötigen das, um zu wissen, ob du in einem Jungs- oder Mädchenzimmer übernachten kannst.")
	private EnumSex sex;
	@NotNull(message = "Bitte gib dein Geburtsdatum an. Damit errechnen wir, ob die Freizeit für dich geeignet ist.")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private LocalDate birthDate;
	@NotBlank(message = "Bitte gib die Strasse deines Wohnsitzes an.")
	private String street;
	@NotBlank(message = "Bitte gib die Postleitzahl deines Wohnsitzes an.")
	private String zip;
	@NotBlank(message = "Bitte gib den Ort deines Wohnsitzes an.")
	private String city;
	@Email(message = "Bitte gib eine gültige E-Mail-Adresse an (oder gar keine, falls du dich ohne Login anmelden möchtest).")
	private String email;
	private String phone;
	private String comment;
	@NotNull(message = "Bitte gib an, zu welcher Freizeit du dich anmelden möchtest.")
	private Integer fkCamp;
	@NotNull(message = "Bitte gib an, in welcher Rolle du dich anmelden möchtest.")
	private EnumCamprole campRole;
	private Boolean registerInKeycloak;
	private String login;
	private String password;
	private String kcForename;
	private String kcSurname;
	private String kcEmail;

	/**
	 * @return forename + surname, separated by a space
	 */
	public String getFullname() {
		return new StringBuilder().append(forename).append(" ").append(surname).toString();
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the sex
	 */
	public EnumSex getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(EnumSex sex) {
		this.sex = sex;
	}

	/**
	 * @return the birthDate
	 */
	public LocalDate getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the registerInKeycloak
	 */
	public Boolean getRegisterInKeycloak() {
		return registerInKeycloak;
	}

	/**
	 * @param registerInKeycloak the registerInKeycloak to set
	 */
	public void setRegisterInKeycloak(Boolean registerInKeycloak) {
		this.registerInKeycloak = registerInKeycloak;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the fkCamp
	 */
	public Integer getFkCamp() {
		return fkCamp;
	}

	/**
	 * @param fkCamp the fkCamp to set
	 */
	public void setFkCamp(Integer fkCamp) {
		this.fkCamp = fkCamp;
	}

	/**
	 * @return the campRole
	 */
	public EnumCamprole getCampRole() {
		return campRole;
	}

	/**
	 * @param campRole the campRole to set
	 */
	public void setCampRole(EnumCamprole campRole) {
		this.campRole = campRole;
	}

	/**
	 * @return the kcForename
	 */
	public String getKcForename() {
		return kcForename;
	}

	/**
	 * @param kcForename the kcForename to set
	 */
	public void setKcForename(String kcForename) {
		this.kcForename = kcForename;
	}

	/**
	 * @return the kcSurname
	 */
	public String getKcSurname() {
		return kcSurname;
	}

	/**
	 * @param kcSurname the kcSurname to set
	 */
	public void setKcSurname(String kcSurname) {
		this.kcSurname = kcSurname;
	}

	/**
	 * @return the kcEmail
	 */
	public String getKcEmail() {
		return kcEmail;
	}

	/**
	 * @param kcEmail the kcEmail to set
	 */
	public void setKcEmail(String kcEmail) {
		this.kcEmail = kcEmail;
	}
}
