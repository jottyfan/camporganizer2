package de.jottyfan.camporganizer.module.registration.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class ProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String forename;
	private String surname;
	private String username;

	public void clear() {
		this.pk = null;
		this.forename = null;
		this.surname = null;
		this.username = null;
	}

	public Boolean getIsEmpty() {
		return pk == null;
	}

	public String getFullname() {
		return new StringBuilder(forename == null ? "" : forename).append(" ").append(surname == null ? "" : surname)
				.toString();
	}

	public String getUsername() {
		return username == null ? null : username.trim();
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}
}
