package de.jottyfan.camporganizer.module.registration.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class CampBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;
	private Integer year;

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}
}
