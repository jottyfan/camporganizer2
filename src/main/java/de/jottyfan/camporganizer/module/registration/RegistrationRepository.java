package de.jottyfan.camporganizer.module.registration;

import static de.jottyfan.camporganizer.db.jooq.Tables.T_CAMP;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_PERSON;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_PERSONDOCUMENT;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_PROFILE;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_PROFILEROLE;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_RSS;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertValuesStep12;
import org.jooq.InsertValuesStep13;
import org.jooq.InsertValuesStep2;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record5;
import org.jooq.Record7;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.jooq.types.DayToSecond;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersonRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersondocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TProfileRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TRssRecord;
import de.jottyfan.camporganizer.module.camplist.model.BookingBean;
import de.jottyfan.camporganizer.module.camplist.model.LambdaResultWrapper;
import de.jottyfan.camporganizer.module.registration.model.CampBean;
import de.jottyfan.camporganizer.module.registration.model.ProfileBean;
import de.jottyfan.camporganizer.module.registration.model.RegistrationBean;

/**
 *
 * @author jotty
 *
 */
@Repository
@Transactional(transactionManager = "transactionManager")
public class RegistrationRepository {
	private static final Logger LOGGER = LogManager.getLogger(RegistrationRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get the camp
	 *
	 * @param pk the ID of the camp
	 * @return the camp bean or null
	 */
	public CampBean getCamp(Integer pk) {
		SelectConditionStep<TCampRecord> sql = jooq.selectFrom(T_CAMP).where(T_CAMP.PK.eq(pk));
		LOGGER.debug(sql.toString());
		TCampRecord r = sql.fetchOne();
		if (r != null) {
			CampBean bean = new CampBean();
			bean.setPk(r.getPk());
			bean.setName(r.getName());
			LocalDateTime arrive = r.getArrive();
			bean.setYear(arrive == null ? null : arrive.getYear());
			return bean;
		} else {
			return null;
		}
	}

	/**
	 * test if the login is available (not yet in use)
	 *
	 * @param login the login
	 * @return true or false
	 */
	public Boolean isLoginNotYetInUse(String login) {
		SelectConditionStep<TProfileRecord> sql = jooq
		// @formatter:off
			.selectFrom(T_PROFILE)
			.where(DSL.lower(T_PROFILE.USERNAME).eq(login));
		// @formatter:on
		LOGGER.debug(sql);
		return sql.fetch().size() < 1;
	}

	/**
	 * save the content in t_person; also, create a profile for the user if
	 * registerInKeycloak is true
	 *
	 * @param bean the bean
	 * @return true or false
	 */
	public Boolean register(RegistrationBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			if (bean.getLogin() != null && !bean.getLogin().isEmpty()) {
				Boolean loginNotYetInUse = isLoginNotYetInUse(bean.getLogin().toLowerCase());
				if (bean.getRegisterInKeycloak() && !loginNotYetInUse) {
					throw new DataAccessException("login already in use: " + bean.getLogin().toLowerCase());
				}
				Integer fkProfile = null;
				if (loginNotYetInUse) {
					String oldPassword = new StrongPasswordEncryptor().encryptPassword(bean.getPassword());
					InsertResultStep<TProfileRecord> sql1 = DSL.using(t)
					// @formatter:off
						.insertInto(T_PROFILE,
								        T_PROFILE.FORENAME,
								        T_PROFILE.SURNAME,
								        T_PROFILE.USERNAME,
								        T_PROFILE.PASSWORD,
								        T_PROFILE.DUEDATE,
								        T_PROFILE.UUID)
						.values(bean.getForename(), bean.getSurname(), bean.getLogin().toLowerCase(), oldPassword, LocalDateTime.now().plus(356, ChronoUnit.DAYS), UUID.nameUUIDFromBytes(bean.getLogin().getBytes()).toString())
						.returning(T_PROFILE.PK);
					// @formatter:on
					LOGGER.debug(sql1.toString());
					fkProfile = sql1.fetchOne().getPk();

					InsertValuesStep2<TRssRecord, String, String> sql2 = jooq
					// @formatter:off
					  .insertInto(T_RSS,
					  		        T_RSS.MSG,
					  		        T_RSS.RECIPIENT)
					  .values(new StringBuilder(bean.getFullname()).append(" hat sich als Nutzer im CampOrganizer2 registriert.").toString(), "admin");
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					sql2.execute();
				} else {
					SelectConditionStep<Record1<Integer>> sql1 = DSL.using(t)
					// @formatter:off
						.select(T_PROFILE.PK)
						.from(T_PROFILE)
						.where(DSL.lower(T_PROFILE.USERNAME).eq(bean.getLogin().toLowerCase()));
					// @formatter:on
					LOGGER.debug(sql1.toString());
					fkProfile = sql1.fetchOne().get(T_PROFILE.PK);
				}
				// register the person for camp participation
				InsertValuesStep13<TPersonRecord, String, String, EnumSex, LocalDate, String, String, String, String, String, EnumCamprole, Integer, String, Integer> sql2 = DSL
						.using(t)
						// @formatter:off
					.insertInto(T_PERSON,
							       T_PERSON.FORENAME,
							       T_PERSON.SURNAME,
							       T_PERSON.SEX,
							       T_PERSON.BIRTHDATE,
							       T_PERSON.STREET,
							       T_PERSON.ZIP,
							       T_PERSON.CITY,
							       T_PERSON.EMAIL,
							       T_PERSON.PHONE,
							       T_PERSON.CAMPROLE,
							       T_PERSON.FK_CAMP,
							       T_PERSON.COMMENT,
							       T_PERSON.FK_PROFILE)
					.values(bean.getForename(), bean.getSurname(), bean.getSex(),
							bean.getBirthDate(), bean.getStreet(), bean.getZip(), bean.getCity(), bean.getEmail(),
							bean.getPhone(), bean.getCampRole(), bean.getFkCamp(), bean.getComment(), fkProfile);
				// @formatter:on
				LOGGER.debug(sql2.toString());
				lrw.add(sql2.execute());
				// register the login for the portal
			} else {
				InsertValuesStep12<TPersonRecord, String, String, EnumSex, LocalDate, String, String, String, String, String, EnumCamprole, Integer, String> sql = DSL
						.using(t)
						// @formatter:off
					.insertInto(T_PERSON,
							       T_PERSON.FORENAME,
							       T_PERSON.SURNAME,
							       T_PERSON.SEX,
							       T_PERSON.BIRTHDATE,
							       T_PERSON.STREET,
							       T_PERSON.ZIP,
							       T_PERSON.CITY,
							       T_PERSON.EMAIL,
							       T_PERSON.PHONE,
							       T_PERSON.CAMPROLE,
							       T_PERSON.FK_CAMP,
							       T_PERSON.COMMENT)
					.values(bean.getForename(), bean.getSurname(), bean.getSex(),
							bean.getBirthDate(), bean.getStreet(), bean.getZip(), bean.getCity(), bean.getEmail(),
							bean.getPhone(), bean.getCampRole(), bean.getFkCamp(), bean.getComment());
				// @formatter:on
				LOGGER.debug(sql.toString());
				lrw.add(sql.execute());
			}
		});
		return lrw.getCounter() > 0;
	}

	/**
	 * get the booking from the database
	 *
	 * @param id the id of the booking
	 * @return the booking bean or null
	 */
	public BookingBean getBooking(Integer id) {
		SelectConditionStep<Record7<Integer, String, String, EnumCamprole, String, LocalDateTime, LocalDateTime>> sql = jooq
		// @formatter:off
			.select(T_PERSON.PK,
					    T_PERSON.FORENAME,
					    T_PERSON.SURNAME,
					    T_PERSON.CAMPROLE,
					    T_CAMP.NAME,
					    T_CAMP.ARRIVE,
					    T_CAMP.DEPART)
			.from(T_PERSON)
			.leftJoin(T_CAMP).on(T_CAMP.PK.eq(T_PERSON.FK_CAMP))
			.where(T_PERSON.PK.eq(id));
		// @formatter:on
		LOGGER.debug(sql.toString());
		Record7<Integer, String, String, EnumCamprole, String, LocalDateTime, LocalDateTime> r = sql.fetchOne();
		if (r != null) {
			BookingBean bean = new BookingBean();
			bean.setPk(r.get(T_PERSON.PK));
			bean.setForename(r.get(T_PERSON.FORENAME));
			bean.setSurname(r.get(T_PERSON.SURNAME));
			bean.setCampName(r.get(T_CAMP.NAME));
			bean.setArrive(r.get(T_CAMP.ARRIVE));
			bean.setDepart(r.get(T_CAMP.DEPART));
			bean.setCamprole(r.get(T_PERSON.CAMPROLE));
			return bean;
		}
		return null;
	}

	/**
	 * remove the booking and all of its dependencies
	 *
	 * @param id the pk of t_person
	 * @return number of affected database rows, should be 1
	 */
	public Integer removeBooking(Integer id) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			SelectConditionStep<Record5<String, String, String, String, LocalDateTime>> sql0 = DSL.using(t)
			// @formatter:off
				.select(T_PROFILE.USERNAME, T_PERSON.FORENAME, T_PERSON.SURNAME, T_CAMP.NAME, T_CAMP.ARRIVE)
				.from(T_PERSON)
				.leftJoin(T_CAMP).on(T_CAMP.PK.eq(T_PERSON.FK_CAMP))
				.leftJoin(T_PROFILE).on(T_PROFILE.PK.eq(T_PERSON.FK_PROFILE))
				.where(T_PERSON.PK.eq(id));
			// @formatter:on
			LOGGER.debug(sql0.toString());
			Record5<String, String, String, String, LocalDateTime> r = sql0.fetchOne();
			if (r == null) {
				throw new DataAccessException("no such entry in t_person with id = " + id);
			}
			String username = r.get(T_PROFILE.USERNAME);
			String forename = r.get(T_PERSON.FORENAME);
			String surname = r.get(T_PERSON.SURNAME);
			String campname = r.get(T_CAMP.NAME);
			LocalDateTime arrive = r.get(T_CAMP.ARRIVE);

			StringBuilder rssMessage = new StringBuilder(username);
			rssMessage.append(" hat die Buchung von ");
			rssMessage.append(forename).append(" ").append(surname);
			rssMessage.append(" an ");
			rssMessage.append(campname).append(" ")
					.append(arrive == null ? "" : arrive.format(DateTimeFormatter.ofPattern("YYYY")));
			rssMessage.append(" storniert.");

			DeleteConditionStep<TPersondocumentRecord> sql1 = DSL.using(t).deleteFrom(T_PERSONDOCUMENT)
					.where(T_PERSONDOCUMENT.FK_PERSON.eq(id));
			LOGGER.debug(sql1.toString());
			sql1.execute();

			DeleteConditionStep<TPersonRecord> sql2 = DSL.using(t).deleteFrom(T_PERSON).where(T_PERSON.PK.eq(id));
			LOGGER.debug(sql2.toString());
			lrw.add(sql2.execute());

			InsertValuesStep2<TRssRecord, String, String> sql3 = DSL.using(t)
			// @formatter:off
				.insertInto(T_RSS,
                    T_RSS.MSG,
                    T_RSS.RECIPIENT)
				.values(rssMessage.toString(), "registrator");
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			sql3.execute();
		});
		return lrw.getCounter();
	}

	/**
	 * returns true if the end date of the camp is not yet over
	 *
	 * @param fkCamp the camp ID
	 * @return true or false
	 */
	public Boolean campIsNotYetOver(Integer fkCamp) {
		SelectConditionStep<Record1<LocalDateTime>> sql = jooq
		// @formatter:off
			.select(T_CAMP.DEPART)
			.from(T_CAMP)
			.where(T_CAMP.PK.eq(fkCamp));
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record1<LocalDateTime> r : sql.fetch()) {
			LocalDateTime depart = r.get(T_CAMP.DEPART);
			LocalDateTime now = LocalDateTime.now();
			return now.isBefore(depart);
		}
		return false;
	}

	/**
	 * remove login
	 *
	 * @param bean containing username of dataset to be removed
	 * @throws DataAccessExceptionF
	 */
	public void removeLogin(ProfileBean bean) throws DataAccessException {
		jooq.transaction(t -> {
			UpdateConditionStep<TPersonRecord> sql = DSL.using(t)
			// @formatter:off
			  .update(T_PERSON)
			  .set(T_PERSON.FK_PROFILE, (Integer) null)
			  .where(T_PERSON.FK_PROFILE.eq(bean.getPk()));
			// @formatter:off
			LOGGER.debug("{}", sql.toString());
			sql.execute();

			DeleteConditionStep<?> sql1 = DSL.using(t)
			// @formatter:off
		    .deleteFrom(T_PROFILEROLE)
		    .where(T_PROFILEROLE.FK_PROFILE.in(
		    		DSL.using(t)
		    		.select(T_PROFILE.PK)
		    		.from(T_PROFILE)
		      .where(T_PROFILE.USERNAME.eq(bean.getUsername())
		    )));
			// @formatter:on
			LOGGER.debug("{}", sql1.toString());
			sql1.execute();

			DeleteConditionStep<?> sql2 = DSL.using(t)
			// @formatter:off
		    .deleteFrom(T_PROFILE)
		    .where(T_PROFILE.USERNAME.eq(bean.getUsername()));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			sql2.execute();

			InsertValuesStep2<TRssRecord, String, String> sql3 = DSL.using(t)
			// @formatter:off
				.insertInto(T_RSS,
						        T_RSS.MSG,
						        T_RSS.RECIPIENT)
				.values(new StringBuilder(bean.getFullname()).append(" hat sich vom Portal CampOrganizer2 abgemeldet.").toString(), "admin");
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			sql3.execute();
		});
	}

	/**
	 * toggle the consent for photo
	 *
	 * @param id the ID of the person
	 */
	public void toggleConsent(Integer id) {
		Boolean consent = jooq.selectFrom(T_PERSON).where(T_PERSON.PK.eq(id)).fetchOne().getConsentCatalogPhoto();
		consent = consent == null ? true : !consent;
		UpdateConditionStep<TPersonRecord> sql = jooq
		// @formatter:off
			.update(T_PERSON)
			.set(T_PERSON.CONSENT_CATALOG_PHOTO, consent)
			.where(T_PERSON.PK.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		sql.execute();
	}

	/**
	 * check if the age of the teacher in campRole fits to the valid ones
	 *
	 * @param birthDate the birth date of the person
	 * @param fkCamp    the ID of the camp
	 * @param campRole  the role; if it is a teacher, check for the age
	 * @return true if it fits, false otherwise
	 */
	public Boolean checkAgeOfTeacher(LocalDate birthDate, Integer fkCamp, EnumCamprole campRole) {
		LocalDateTime birthDateDay = birthDate.atStartOfDay();
		if (EnumCamprole.teacher.equals(campRole)) {
			SelectConditionStep<Record2<Integer, DayToSecond>> sql = jooq
			// @formatter:off
				.select(T_CAMP.MAX_AGE,
								DSL.localDateTimeDiff(T_CAMP.DEPART, birthDateDay).as("teacherAge"))
				.from(T_CAMP)
				.where(T_CAMP.PK.eq(fkCamp));
			// @formatter:on
			LOGGER.debug(sql.toString());
			Record r = sql.fetchOne();
			Integer minTeacherAge = r.get(T_CAMP.MAX_AGE) + 2; // by default, we need 2 years older teachers at least
			DayToSecond currentTeacherAge = r.get("teacherAge", DayToSecond.class);
			double totalYears = currentTeacherAge.getTotalDays() / 365.25; // in years
			int years = (int) totalYears;
			return years >= minTeacherAge;
		} else {
			return true;
		}
	}
}
