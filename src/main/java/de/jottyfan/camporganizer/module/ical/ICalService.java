package de.jottyfan.camporganizer.module.ical;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import biweekly.Biweekly;
import biweekly.ICalendar;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author jotty
 *
 */
@Service
public class ICalService {

	@Autowired
	private ICalRepository gateway;

	public Boolean generate(HttpServletResponse response) throws IOException {
		ICalendar ical = gateway.getIcal();
		String content = Biweekly.write(ical).go();

		response.setHeader("charset", "UTF-8");
		response.setHeader("Content-Type", "text/calendar");
		response.setHeader("Content-Disposition", "attachment; filename=\"onkelwernerfreizeiten.de.ics\"");

		response.getWriter().write(content);
		response.getWriter().flush();
		response.flushBuffer();

		return true;
	}
}
