package de.jottyfan.camporganizer.module.ical;

import java.io.IOException;

import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.camporganizer.module.camplist.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class ICalController extends CommonController {

	@Autowired
	private ICalService service;

	/**
	 * generate the ical response stream
	 *
	 * @throws IOException on io errors
	 */
	@GetMapping("/ical")
	public void generate(HttpServletResponse response) throws IOException {
		service.generate(response);
	}
}
