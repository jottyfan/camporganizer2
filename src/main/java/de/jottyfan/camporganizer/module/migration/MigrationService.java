package de.jottyfan.camporganizer.module.migration;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.module.registration.KeycloakRepository;

/**
 *
 * @author jotty
 *
 */
@Service
public class MigrationService {

	@Autowired
	private MigrationRepository repository;

	@Autowired
	private KeycloakRepository keycloak;

	/**
	 * check if the login is valid
	 *
	 * @param bean the bean
	 * @return true or false
	 */
	public boolean checkLogin(@Valid MigrationBean bean) {
		String encryptedPassword = repository.getEncryptedPassword(bean.getUsername());
		return bean.checkPassword(encryptedPassword);
	}

	/**
	 * do the migration
	 *
	 * @param bean the bean
	 * @return true or false
	 */
	public boolean migrate(@Valid MigrationBean bean) {
		return keycloak.register(bean.getForename(), bean.getSurname(), bean.getUsername(), bean.getPassword(), bean.getEmail());
	}
}
