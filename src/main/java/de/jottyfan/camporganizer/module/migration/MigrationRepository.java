package de.jottyfan.camporganizer.module.migration;

import static de.jottyfan.camporganizer.db.jooq.Tables.T_PROFILE;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jotty
 *
 */
@Repository
@Transactional(transactionManager = "transactionManager")
public class MigrationRepository {
	private static final Logger LOGGER = LogManager.getLogger(MigrationRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get the encrypted password of username
	 *
	 * @param username the username
	 * @return the encrypted password or null
	 */
	public String getEncryptedPassword(String username) {
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(T_PROFILE.PASSWORD)
			.from(T_PROFILE)
			.where(T_PROFILE.USERNAME.eq(username));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.fetchOne(T_PROFILE.PASSWORD);
	}
}
