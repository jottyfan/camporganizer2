package de.jottyfan.camporganizer.module.migration;

import java.io.Serializable;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.password.StrongPasswordEncryptor;

import de.jottyfan.camporganizer.module.registration.validate.UnusedUsername;

/**
 *
 * @author jotty
 *
 */
@UnusedUsername(field = "email", message = "Diese E-Mail ist als Login leider bereits vergeben. Bitte wähle eine andere.")
public class MigrationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Bitte gib deinen Vornamen an.")
	private String forename;
	@NotBlank(message = "Bitte gib deinen Nachnamen an.")
	private String surname;
	@NotBlank(message = "Bitte gib deinen alten Nutzernamen an.")
	private String username;
	@NotBlank(message = "Bitte gib dein altes Passwort an.")
	private String password;
	@NotBlank(message = "Bitte gib eine E-Mail-Adresse an. Ohne kannst du deinen Zugang nicht umziehen.")
	@Email(message = "Bitte gib eine gültige E-Mail-Adresse an.")
	private String email;

	/**
	 * check if the current password fits to the encrypted one
	 *
	 * @param encryptedPassword the encrypted password from the database
	 * @return true or false
	 */
	public boolean checkPassword(String encryptedPassword) {
		try {
			return new StrongPasswordEncryptor().checkPassword(password, encryptedPassword);
		} catch (EncryptionOperationNotPossibleException e) {
			return false; // password is not decryptable
		}
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

}
