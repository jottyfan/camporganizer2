package de.jottyfan.camporganizer.module.confirmation.person;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.module.confirmation.person.model.CampBean;
import de.jottyfan.camporganizer.module.confirmation.person.model.PersonBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class PersonService {
	@Autowired
	private PersonRepository gateway;

	public PersonBean getPerson(String username, Integer pk) {
		return gateway.getPerson(username, pk);
	}

	public Integer updatePerson(PersonBean bean, String worker) {
		return gateway.updatePerson(bean, worker);
	}

	public List<CampBean> getCamps(String username) {
		return gateway.getCamps(username);
	}

	public String getAnnotations(Integer pk) {
		return gateway.getAnnotations(pk);
	}
}
