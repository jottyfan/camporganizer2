package de.jottyfan.camporganizer.module.confirmation.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.jottyfan.camporganizer.module.camplist.CommonController;
import de.jottyfan.camporganizer.module.confirmation.confirmation.ConfirmationService;
import de.jottyfan.camporganizer.module.confirmation.person.model.PersonBean;
import jakarta.servlet.http.HttpServletRequest;

/**
 *
 * @author jotty
 *
 */
@Controller
public class PersonController extends CommonController {

	@Autowired
	private PersonService personService;

	@GetMapping("/confirmation/person/{pk}")
	public String getIndex(Model model, @PathVariable Integer pk) {
		String username = super.getCurrentUser();
		model.addAttribute("currentUser", username);
		model.addAttribute("person", personService.getPerson(username, pk));
		model.addAttribute("camps", personService.getCamps(username));
		model.addAttribute("annotations", personService.getAnnotations(pk));
		return "confirmation/person";
	}

	@PostMapping("/confirmation/person/update")
	public String doUpdate(@ModelAttribute PersonBean bean, Model model) {
		String username = super.getCurrentUser();
		personService.updatePerson(bean, username);
		return "redirect:/confirmation";
	}
}
