package de.jottyfan.camporganizer.module.confirmation.person.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;

/**
 *
 * @author jotty
 *
 */
public class PersonBean implements Serializable {
	private static final long serialVersionUID = 2L;

	private Integer pk;
	private String forename;
	private String surname;
	private String street;
	private String zip;
	private String city;
	private String phone;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthdate;
	private EnumCamprole camprole;
	private String email;
	private Integer fkCamp;
	private Integer fkProfile;
	private Boolean accept;
	private LocalDateTime created;
	private EnumSex sex;
	private Integer fkRegistrator;
	private String comment;

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the birthdate
	 */
	public LocalDate getBirthdate() {
		return birthdate;
	}

	/**
	 * @param birthdate the birthdate to set
	 */
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * @return the camprole
	 */
	public EnumCamprole getCamprole() {
		return camprole;
	}

	/**
	 * @param camprole the camprole to set
	 */
	public void setCamprole(EnumCamprole camprole) {
		this.camprole = camprole;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the fkCamp
	 */
	public Integer getFkCamp() {
		return fkCamp;
	}

	/**
	 * @param fkCamp the fkCamp to set
	 */
	public void setFkCamp(Integer fkCamp) {
		this.fkCamp = fkCamp;
	}

	/**
	 * @return the fkProfile
	 */
	public Integer getFkProfile() {
		return fkProfile;
	}

	/**
	 * @param fkProfile the fkProfile to set
	 */
	public void setFkProfile(Integer fkProfile) {
		this.fkProfile = fkProfile;
	}

	/**
	 * @return the accept
	 */
	public Boolean getAccept() {
		return accept;
	}

	/**
	 * @param accept the accept to set
	 */
	public void setAccept(Boolean accept) {
		this.accept = accept;
	}

	/**
	 * @return the created
	 */
	public LocalDateTime getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	/**
	 * @return the sex
	 */
	public EnumSex getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(EnumSex sex) {
		this.sex = sex;
	}

	/**
	 * @return the fkRegistrator
	 */
	public Integer getFkRegistrator() {
		return fkRegistrator;
	}

	/**
	 * @param fkRegistrator the fkRegistrator to set
	 */
	public void setFkRegistrator(Integer fkRegistrator) {
		this.fkRegistrator = fkRegistrator;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}
}
