package de.jottyfan.camporganizer.module.confirmation.confirmation.model;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author jotty
 *
 */
public class CampOverviewBean implements Serializable, Comparable<CampOverviewBean> {
	private static final long serialVersionUID = 1L;

	private final LocalDate date;
	private final String camp;
	private Integer approved;
	private Integer rejected;
	private Integer untouched;

	public CampOverviewBean(LocalDate date, String camp) {
		this.date = date;
		this.camp = camp;
		this.approved = 0;
		this.rejected = 0;
		this.untouched = 0;
	}

	@Override
	public int compareTo(CampOverviewBean bean) {
		return date == null || bean == null || bean.getDate() == null ? 0 : bean.getDate().compareTo(date);
	}

	/**
	 * @return the camp
	 */
	public String getCamp() {
		return camp;
	}

	/**
	 * @return the approved
	 */
	public Integer getApproved() {
		return approved;
	}

	/**
	 * @param approved the approved to set
	 */
	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	/**
	 * @return the rejected
	 */
	public Integer getRejected() {
		return rejected;
	}

	/**
	 * @param rejected the rejected to set
	 */
	public void setRejected(Integer rejected) {
		this.rejected = rejected;
	}

	/**
	 * @return the untouched
	 */
	public Integer getUntouched() {
		return untouched;
	}

	/**
	 * @param untouched the untouched to set
	 */
	public void setUntouched(Integer untouched) {
		this.untouched = untouched;
	}

	/**
	 * @return the date
	 */
	public LocalDate getDate() {
		return date;
	}
}
