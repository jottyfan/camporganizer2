package de.jottyfan.camporganizer.module.confirmation.confirmation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import de.jottyfan.camporganizer.module.camplist.CommonController;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.websocket.server.PathParam;

/**
 *
 * @author jotty
 *
 */
@Controller
public class SearchController extends CommonController {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private ConfirmationService service;

	@GetMapping("/confirmation/search")
	@ResponseBody
	public String search(@PathParam(value = "needle") String needle, Model model) {
		return service.search(needle, request.getRequestURI().replace("search", "person"), super.getCurrentUser());
	}
}
