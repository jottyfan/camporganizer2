package de.jottyfan.camporganizer.module.confirmation.person.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class CampBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;
	private LocalDateTime arrive;
	private String location;

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the arrive
	 */
	public LocalDateTime getArrive() {
		return arrive;
	}

	/**
	 * @param arrive the arrive to set
	 */
	public void setArrive(LocalDateTime arrive) {
		this.arrive = arrive;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
}
