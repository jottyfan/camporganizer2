package de.jottyfan.camporganizer.module.confirmation.confirmation.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;

/**
 *
 * @author jotty
 *
 */
public class BookingBean implements Serializable, Comparable<BookingBean> {
	private static final long serialVersionUID = 3L;

	private final Integer pkPerson;
	private final LocalDate date;
	private final String camp;
	private String fullname;
	private String role;
	private LocalDateTime registered;
	private Boolean accept;

	public BookingBean(Integer pkPerson, LocalDate date, String camp) {
		this.pkPerson = pkPerson;
		this.date = date;
		this.camp = camp;
	}

	@Override
	public int compareTo(BookingBean bean) {
		return date == null || bean == null || bean.getDate() == null ? 0 : bean.getDate().compareTo(date);
	}

	/**
	 * get the German role name
	 *
	 * @return the German role name
	 */
	public String getRolename() {
		if (EnumCamprole.student.getLiteral().equals(role)) {
			return "Teilnehmer";
		} else if (EnumCamprole.teacher.getLiteral().equals(role)) {
			return "Mitarbeiter";
		} else if (EnumCamprole.director.getLiteral().equals(role)) {
			return "Leiter";
		} else if (EnumCamprole.feeder.getLiteral().equals(role)) {
			return "Küchenteam";
		} else if (EnumCamprole.observer.getLiteral().equals(role)) {
			return "Mitarbeiterkind";
		} else {
			return role;
		}
	}

	/**
	 * @return the fullname
	 */
	public String getFullname() {
		return fullname;
	}

	/**
	 * @param fullname the fullname to set
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the registered
	 */
	public LocalDateTime getRegistered() {
		return registered;
	}

	/**
	 * @param registered the registered to set
	 */
	public void setRegistered(LocalDateTime registered) {
		this.registered = registered;
	}

	/**
	 * @return the date
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * @return the camp
	 */
	public String getCamp() {
		return camp;
	}

	public Integer getPkPerson() {
		return pkPerson;
	}

	public Boolean getAccept() {
		return accept;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept;
	}
}
