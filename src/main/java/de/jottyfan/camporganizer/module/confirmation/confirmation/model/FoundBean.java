package de.jottyfan.camporganizer.module.confirmation.confirmation.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class FoundBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer fkPerson;
	private String forename;
	private String surname;
	private String campname;
	private Integer year;

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the campname
	 */
	public String getCampname() {
		return campname;
	}

	/**
	 * @param campname the campname to set
	 */
	public void setCampname(String campname) {
		this.campname = campname;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getFkPerson() {
		return fkPerson;
	}

	public void setFkPerson(Integer fkPerson) {
		this.fkPerson = fkPerson;
	}

}
