package de.jottyfan.camporganizer.module.confirmation.confirmation;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.camporganizer.module.camplist.CommonController;
import de.jottyfan.camporganizer.module.confirmation.confirmation.model.CampOverviewBean;

/**
 *
 * @author jotty
 *
 */
@Controller
public class ConfirmationController extends CommonController {

	@Autowired
	private ConfirmationService indexService;

	@GetMapping("/confirmation")
	public String getIndex(Model model) {
		List<CampOverviewBean> campoverview = indexService.getCampOverview(super.getCurrentUser());
		CampOverviewBean campoverviewsummary = new CampOverviewBean(LocalDate.now(), "summary");
		for (CampOverviewBean bean : campoverview) {
			campoverviewsummary.setApproved(bean.getApproved() + campoverviewsummary.getApproved());
			campoverviewsummary.setRejected(bean.getRejected() + campoverviewsummary.getRejected());
			campoverviewsummary.setUntouched(bean.getUntouched() + campoverviewsummary.getUntouched());
		}
		model.addAttribute("campoverview", campoverview);
		model.addAttribute("campoverviewsummary", campoverviewsummary);
		model.addAttribute("untouched", indexService.getUntouched(super.getCurrentUser()));
		model.addAttribute("approved", indexService.getApproved(super.getCurrentUser()));
		model.addAttribute("rejected", indexService.getRejected(super.getCurrentUser()));
		return "confirmation/confirmation";
	}
}
