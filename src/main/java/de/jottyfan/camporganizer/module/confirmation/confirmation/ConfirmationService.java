package de.jottyfan.camporganizer.module.confirmation.confirmation;

import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.jooq.exception.DataAccessException;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.module.confirmation.confirmation.model.BookingBean;
import de.jottyfan.camporganizer.module.confirmation.confirmation.model.CampOverviewBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class ConfirmationService {
	@Autowired
	private ConfirmationRepository gateway;

	public List<CampOverviewBean> getCampOverview(String currentUser) {
		return gateway.getCampOverviewBeans(currentUser);
	}

	public List<BookingBean> getUntouched(String currentUser) {
		return gateway.getUntouched(currentUser);
	}

	public List<BookingBean> getApproved(String currentUser) {
		return gateway.getApproved(currentUser);
	}

	public List<BookingBean> getRejected(String currentUser) {
		return gateway.getRejected(currentUser);
	}

	public String search(String needle, String linkURL, String currentUser) {
		StringBuilder buf = new StringBuilder(
				"<table class=\"table table-striped\"><thead><tr><th>Dabei</th><th>Name</th><th>Freizeit</th><th>Rolle</th></tr><tbody>");
		for (BookingBean bean : gateway.getSearchResult(needle, currentUser)) {
			String acceptHtml = "";
			if (bean.getAccept() == null) {
				acceptHtml = "<i class=\"fas fa-question framed framed-orange\"></i>";
			} else if (bean.getAccept()) {
				acceptHtml = "<i class=\"fas fa-check framed framed-green\"></i>";
			} else {
				acceptHtml = "<i class=\"fas fa-ban framed framed-red\"></i>";
			}
			buf.append(String.format("<tr><td>%s</td><td><a href=\"%s/%d\">%s</a></td><td>%s</td><td>%s</td></tr>",
					acceptHtml, linkURL, bean.getPkPerson(), bean.getFullname(), bean.getCamp(), bean.getRolename()));
		}
		buf.append("</tbody></table>");
		return buf.toString();
	}
}
