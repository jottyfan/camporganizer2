package de.jottyfan.camporganizer.module.admin.model;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class IntKeyValueBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer key;
	private String value;

	public static final IntKeyValueBean of(Integer key, String value) {
		IntKeyValueBean bean = new IntKeyValueBean();
		bean.setKey(key);
		bean.setValue(value);
		return bean;
	}

	/**
	 * @return the key
	 */
	public Integer getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(Integer key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
