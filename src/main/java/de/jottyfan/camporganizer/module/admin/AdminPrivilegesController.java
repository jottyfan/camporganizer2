package de.jottyfan.camporganizer.module.admin;

import java.util.List;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.jottyfan.camporganizer.db.jooq.enums.EnumModule;
import de.jottyfan.camporganizer.module.admin.model.CampBean;
import de.jottyfan.camporganizer.module.admin.model.CampProfileBean;
import de.jottyfan.camporganizer.module.admin.model.ProfileBean;
import de.jottyfan.camporganizer.module.camplist.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class AdminPrivilegesController extends CommonController {

	@Autowired
	private AdminPrivilegesService service;

	@GetMapping("/admin/privileges/userbased")
	public String getUserbased(Model model) {
		model.addAttribute("list", service.getProfiles());
		return "/admin/privileges/userbased";
	}

	@GetMapping("/admin/privileges/rolebased")
	public String getRolebased(Model model) {
		model.addAttribute("list", service.getAllModules());
		return "/admin/privileges/rolebased";
	}

	@GetMapping("/admin/privileges/campbased")
	public String getCampbased(Model model) {
		model.addAttribute("list", service.getAllCamps());
		return "/admin/privileges/campbased";
	}

	@GetMapping("/admin/privileges/rolebased/{role}")
	public String getRolebased(@PathVariable String role, Model model) {
		model.addAttribute("list", service.getAllModules());
		model.addAttribute("selected", role);
		model.addAttribute("container", service.getPersonCampMappingByModule(EnumModule.valueOf(role)));
		model.addAttribute("pagedest", "_admin_privileges_rolebased_" + role);
		return "/admin/privileges/rolebased";
	}

	@GetMapping("/admin/privileges/campbased/{campid}")
	public String getCampbased(@PathVariable Integer campid, Model model) {
		List<CampBean> list = service.getAllCamps();
		model.addAttribute("list", list);
		String campname = "?";
		for (CampBean p : list) {
			if (p.getPk().equals(campid)) {
				campname = p.getFullname();
			}
		}
		model.addAttribute("selected", campname);
		model.addAttribute("container", service.getPersonModuleMappingByCamp(campid));
		model.addAttribute("pagedest", "_admin_privileges_campbased_" + campid);
		return "/admin/privileges/campbased";
	}

	@GetMapping("/admin/privileges/userbased/{userid}")
	public String getUserbased(@PathVariable Integer userid, Model model) {
		List<ProfileBean> list = service.getProfiles();
		model.addAttribute("list", list);
		String selected = "?";
		for (ProfileBean p : list) {
			if (p.getPk().equals(userid)) {
				selected = p.getFullname();
			}
		}
		model.addAttribute("selected", selected);
		model.addAttribute("container", service.getModuleCampMappingByPerson(userid));
		model.addAttribute("pagedest", "_admin_privileges_userbased_" + userid);
		return "/admin/privileges/userbased";
	}

	@GetMapping("/admin/privileges/delete/{id}/{pagedest}")
	public String deleteFromCampProfile(@PathVariable Integer id, @PathVariable String pagedest) {
		service.deleteFromCampProfile(id);
		return "redirect:" +  pagedest.replace("_", "/");
	}

	@GetMapping("/admin/privileges/add/{pagedest}")
	public String prepareAdd(Model model, @PathVariable String pagedest) {
		model.addAttribute("pagedest", pagedest);
		model.addAttribute("bean", new CampProfileBean());
		model.addAttribute("profiles", service.getProfiles());
		model.addAttribute("camps", service.getAllCamps());
		model.addAttribute("modules", service.getAllModules());
		return "/admin/privileges/add";
	}

	@PostMapping("/admin/privileges/insert/{pagedest}")
	public String insertCampProfile(@Valid @ModelAttribute("bean") CampProfileBean bean, final BindingResult bindingResult, @PathVariable String pagedest, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("pagedest", pagedest);
			model.addAttribute("profiles", service.getProfiles());
			model.addAttribute("camps", service.getAllCamps());
			model.addAttribute("modules", service.getAllModules());
			return "/admin/privileges/add";
		}
		service.insertIntoCampProfile(bean);
		return "redirect:" + pagedest.replace("_", "/");
	}

	@GetMapping("/admin/privileges/abortinsert/{pagedest}")
	public String abortInsert(@PathVariable String pagedest) {
		return "redirect:" + pagedest.replace("_", "/");
	}
}
