package de.jottyfan.camporganizer.module.admin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jotty
 *
 */
public class PrivilegesContainerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<String, List<IntKeyValueBean>> map;

	public PrivilegesContainerBean() {
		map = new HashMap<>();
	}

	/**
	 * get the content of key
	 *
	 * @param key the key
	 * @return the value
	 */
	public List<IntKeyValueBean> get(String key) {
		return map == null ? null : map.get(key);
	}

	/**
	 * add the value to the key
	 *
	 * @param key   the key
	 * @param value the value
	 */
	public void add(String key, IntKeyValueBean value) {
		if (get(key) != null) {
			map.get(key).add(value);
		} else {
			throw new NullPointerException("key not found");
		}
	}

	/**
	 * add the key to this map
	 *
	 * @param key
	 */
	public void addKey(String key) {
		map.put(key, new ArrayList<>());
	}

	/**
	 * @return the map
	 */
	public Map<String, List<IntKeyValueBean>> getMap() {
		return map;
	}
}
