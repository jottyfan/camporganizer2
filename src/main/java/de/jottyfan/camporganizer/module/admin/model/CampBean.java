package de.jottyfan.camporganizer.module.admin.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import de.jottyfan.camporganizer.db.jooq.tables.records.TCampRecord;

/**
 *
 * @author jotty
 *
 */
public class CampBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	@NotBlank
	private String name;
	@NotNull
	private Integer fkDocument;
	@NotNull
	private Integer fkLocation;
	@NotNull
	private Integer fkProfile;
	private Boolean lockSales;
	@NotNull
	private Integer maxAge;
	@NotNull
	private Integer minAge;
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate arrive;
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate depart;
	private String countries;
	@NotNull
	private String price;

	/**
	 * generate a camp bean out of r
	 *
	 * @param r the record
	 * @return the camp bean
	 */
	public static final CampBean of(TCampRecord r) {
		if (r == null) {
			return null;
		}
		CampBean bean = new CampBean();
		LocalDateTime arrive = r.getArrive();
		LocalDateTime depart = r.getDepart();
		bean.setArrive(arrive == null ? null : arrive.toLocalDate());
		bean.setCountries(r.getCountries());
		bean.setDepart(depart == null ? null : depart.toLocalDate());
		bean.setFkDocument(r.getFkDocument());
		bean.setFkLocation(r.getFkLocation());
		bean.setFkProfile(r.getFkProfile());
		bean.setLockSales(r.getLockSales());
		bean.setMaxAge(r.getMaxAge());
		bean.setMinAge(r.getMinAge());
		bean.setName(r.getName());
		bean.setPk(r.getPk());
		bean.setPrice(r.getPrice());
		return bean;
	}

	public String getFullname() {
		return new StringBuilder().append(name).append(" ").append(arrive == null ? "?" : arrive.format(DateTimeFormatter.ofPattern("yyyy"))).toString();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fkDocument
	 */
	public Integer getFkDocument() {
		return fkDocument;
	}

	/**
	 * @param fkDocument the fkDocument to set
	 */
	public void setFkDocument(Integer fkDocument) {
		this.fkDocument = fkDocument;
	}

	/**
	 * @return the fkLocation
	 */
	public Integer getFkLocation() {
		return fkLocation;
	}

	/**
	 * @param fkLocation the fkLocation to set
	 */
	public void setFkLocation(Integer fkLocation) {
		this.fkLocation = fkLocation;
	}

	/**
	 * @return the fkProfile
	 */
	public Integer getFkProfile() {
		return fkProfile;
	}

	/**
	 * @param fkProfile the fkProfile to set
	 */
	public void setFkProfile(Integer fkProfile) {
		this.fkProfile = fkProfile;
	}

	/**
	 * @return the lockSales
	 */
	public Boolean getLockSales() {
		return lockSales;
	}

	/**
	 * @param lockSales the lockSales to set
	 */
	public void setLockSales(Boolean lockSales) {
		this.lockSales = lockSales;
	}

	/**
	 * @return the maxAge
	 */
	public Integer getMaxAge() {
		return maxAge;
	}

	/**
	 * @param maxAge the maxAge to set
	 */
	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	/**
	 * @return the minAge
	 */
	public Integer getMinAge() {
		return minAge;
	}

	/**
	 * @param minAge the minAge to set
	 */
	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	/**
	 * @return the arrive
	 */
	public LocalDate getArrive() {
		return arrive;
	}

	/**
	 * @param arrive the arrive to set
	 */
	public void setArrive(LocalDate arrive) {
		this.arrive = arrive;
	}

	/**
	 * @return the depart
	 */
	public LocalDate getDepart() {
		return depart;
	}

	/**
	 * @param depart the depart to set
	 */
	public void setDepart(LocalDate depart) {
		this.depart = depart;
	}

	/**
	 * @return the countries
	 */
	public String getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public void setCountries(String countries) {
		this.countries = countries;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the countriesList
	 */
	public List<String> getCountriesList() {
		String[] splitted = countries == null ? null : countries.split(",");
		List<String> list = new ArrayList<>();
		if (splitted != null && splitted.length > 0) {
			for (String s : splitted) {
				list.add(s.trim());
			}
		}
		return list;
	}

	/**
	 * @param countriesList the countriesList to set
	 */
	public void setCountriesList(List<String> countriesList) {
		StringBuilder buf = new StringBuilder();
		boolean first = true;
		for (String s : countriesList) {
			buf.append(first ? "" : ", ").append(s);
			first = false;
		}
		this.countries = buf.toString();
	}
}
