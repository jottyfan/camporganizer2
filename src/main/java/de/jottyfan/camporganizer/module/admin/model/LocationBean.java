package de.jottyfan.camporganizer.module.admin.model;

import java.io.Serializable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import de.jottyfan.camporganizer.db.jooq.tables.records.TLocationRecord;

/**
 *
 * @author jotty
 *
 */
public class LocationBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;

	@NotBlank
	private String name;
	@NotNull
	private Integer fkDocument;
	@NotBlank
	private String url;

	public static LocationBean of(TLocationRecord r) {
		if (r == null) {
			return null;
		}
		LocationBean bean = new LocationBean();
		bean.setPk(r.getPk());
		bean.setName(r.getName());
		bean.setFkDocument(r.getFkDocument());
		bean.setUrl(r.getUrl());
		return bean;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fkDocument
	 */
	public Integer getFkDocument() {
		return fkDocument;
	}

	/**
	 * @param fkDocument the fkDocument to set
	 */
	public void setFkDocument(Integer fkDocument) {
		this.fkDocument = fkDocument;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
