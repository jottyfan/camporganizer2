package de.jottyfan.camporganizer.module.admin.model;

import java.io.Serializable;

import de.jottyfan.camporganizer.db.jooq.tables.records.TProfileRecord;

/**
 *
 * @author jotty
 *
 */
public class ProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String forename;
	private String surname;

	public static final ProfileBean of(TProfileRecord r) {
		if (r == null) {
			return null;
		}
		ProfileBean bean = new ProfileBean();
		bean.setPk(r.getPk());
		bean.setForename(r.getForename());
		bean.setSurname(r.getSurname());
		return bean;
	}

	public String getFullname() {
		return new StringBuilder().append(forename).append(" ").append(surname).toString();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
}
