package de.jottyfan.camporganizer.module.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.camporganizer.db.jooq.enums.EnumModule;
import de.jottyfan.camporganizer.module.admin.model.CampBean;
import de.jottyfan.camporganizer.module.admin.model.CampProfileBean;
import de.jottyfan.camporganizer.module.admin.model.PrivilegesContainerBean;
import de.jottyfan.camporganizer.module.admin.model.ProfileBean;

/**
 *
 * @author jotty
 *
 */
@Service
public class AdminPrivilegesService {

	@Autowired
	private AdminRepository adminRepository;

	/**
	 * get all camp beans from the database
	 *
	 * @return all camp beans; an empty list at least
	 */
	public List<CampBean> getAllCamps() {
		return adminRepository.getAllCamps();
	}

	/**
	 * get all profiles from the db
	 *
	 * @return the profiles
	 */
	public List<ProfileBean> getProfiles() {
		return adminRepository.getProfiles();
	}

	/**
	 * get all roles
	 *
	 * @return all roles
	 */
	public List<String> getAllModules() {
		return adminRepository.getAllModules();
	}

	/**
	 * get the mapping that contains the form bean for a selected module
	 *
	 * @param module the module
	 * @return the container
	 */
	public PrivilegesContainerBean getPersonCampMappingByModule(EnumModule module) {
		return adminRepository.getPersonCampMappingByModule(module);
	}

	/**
	 * get the mapping that contains the form bean for a selected camp
	 *
	 * @param camp the camp ID
	 * @return the container
	 */
	public PrivilegesContainerBean getPersonModuleMappingByCamp(Integer camp) {
		return adminRepository.getPersonModuleMappingByCamp(camp);
	}

	/**
	 * get the mapping that contains the form bean for a selected user
	 *
	 * @param user the user ID
	 * @return the container
	 */
	public PrivilegesContainerBean getModuleCampMappingByPerson(Integer user) {
		return adminRepository.getModuleCampMappingByPerson(user);
	}

	/**
	 * delete from camp profile
	 *
	 * @param id the ID
	 */
	public void deleteFromCampProfile(Integer id) {
		adminRepository.deleteFromCampProfile(id);
	}

	/**
	 * add the camp profile to the database
	 *
	 * @param bean the bean
	 */
	public void insertIntoCampProfile(CampProfileBean bean) {
		EnumModule m = bean.getModule() == null ? null : EnumModule.valueOf(bean.getModule());
		adminRepository.addToCampProfile(bean.getFkCamp(), bean.getFkProfile(), m);
	}
}
