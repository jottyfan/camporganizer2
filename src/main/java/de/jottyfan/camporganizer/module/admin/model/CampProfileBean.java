package de.jottyfan.camporganizer.module.admin.model;

import java.io.Serializable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 *
 * @author jotty
 *
 */
public class CampProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;

	@NotNull
	private Integer fkCamp;
	@NotNull
	private Integer fkProfile;
	@NotBlank
	private String module;

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the fkCamp
	 */
	public Integer getFkCamp() {
		return fkCamp;
	}

	/**
	 * @param fkCamp the fkCamp to set
	 */
	public void setFkCamp(Integer fkCamp) {
		this.fkCamp = fkCamp;
	}

	/**
	 * @return the fkProfile
	 */
	public Integer getFkProfile() {
		return fkProfile;
	}

	/**
	 * @param fkProfile the fkProfile to set
	 */
	public void setFkProfile(Integer fkProfile) {
		this.fkProfile = fkProfile;
	}

	/**
	 * @return the module
	 */
	public String getModule() {
		return module;
	}

	/**
	 * @param module the module to set
	 */
	public void setModule(String module) {
		this.module = module;
	}
}
