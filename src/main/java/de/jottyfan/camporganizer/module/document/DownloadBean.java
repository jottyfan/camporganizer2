package de.jottyfan.camporganizer.module.document;

import java.io.Serializable;

import de.jottyfan.camporganizer.db.jooq.enums.EnumFiletype;

/**
 *
 * @author jotty
 *
 */
public class DownloadBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private EnumFiletype filetype;
	private String content;

	/**
	 * @return the filetype
	 */
	public EnumFiletype getFiletype() {
		return filetype;
	}

	/**
	 * @param filetype the filetype to set
	 */
	public void setFiletype(EnumFiletype filetype) {
		this.filetype = filetype;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
