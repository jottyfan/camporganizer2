package de.jottyfan.camporganizer.module.document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jotty
 *
 */
@Service
public class DocumentService {

	@Autowired
	private DocumentRepository repository;

	/**
	 * get the document of id
	 *
	 * @param id the id of the document
	 */
	public DownloadBean getDocument(Integer id) {
		return repository.getDocument(id);
	}
}
