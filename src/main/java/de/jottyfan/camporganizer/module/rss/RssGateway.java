package de.jottyfan.camporganizer.module.rss;

import static de.jottyfan.camporganizer.db.jooq.Tables.T_RSS;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.camporganizer.db.jooq.tables.records.TRssRecord;

/**
 *
 * @author jotty
 *
 */
@Repository
@Transactional(transactionManager = "transactionManager")
public class RssGateway {

	private static final Logger LOGGER = LogManager.getLogger(RssGateway.class);

	@Autowired
	private DSLContext jooq;

	public List<RssBean> getRss(String recipientCode) throws DataAccessException {
		SelectConditionStep<Record3<Integer, String, LocalDateTime>> sql = jooq
		// @formatter:off
		  .select(T_RSS.PK,
	  		      T_RSS.MSG,
		  	      T_RSS.REGDATE)
		  .from(T_RSS)
		  .where(T_RSS.RECIPIENT.eq(recipientCode));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<RssBean> list = new ArrayList<>();
		for (Record3<Integer, String, LocalDateTime> r : sql.fetch()) {
			RssBean bean = new RssBean(r.get(T_RSS.PK));
			bean.setRecipient(recipientCode);
			bean.setMessage(r.get(T_RSS.MSG));
			bean.setPubdate(r.get(T_RSS.REGDATE));
			list.add(bean);
		}
		return list;
	}

	public List<String> getAllFeeds() {
		SelectJoinStep<Record1<String>> sql = jooq.selectDistinct(T_RSS.RECIPIENT).from(T_RSS);
		LOGGER.debug(sql.toString());
		return sql.fetch(T_RSS.RECIPIENT);
	}

	public List<RssBean> getAllRss() throws DataAccessException {
		SelectJoinStep<Record4<Integer, String, String, LocalDateTime>> sql = jooq
		// @formatter:off
		  .select(T_RSS.PK,
	  		      T_RSS.RECIPIENT,
	  		      T_RSS.MSG,
		  	      T_RSS.REGDATE)
		  .from(T_RSS);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<RssBean> list = new ArrayList<>();
		for (Record4<Integer, String, String, LocalDateTime> r : sql.fetch()) {
			RssBean bean = new RssBean(r.get(T_RSS.PK));
			bean.setRecipient(r.get(T_RSS.RECIPIENT));
			bean.setMessage(r.get(T_RSS.MSG));
			bean.setPubdate(r.get(T_RSS.REGDATE));
			list.add(bean);
		}
		return list;
	}

	public void deleteRss(RssBean bean) throws DataAccessException {
		DeleteConditionStep<TRssRecord> sql = jooq
		// @formatter:off
			.deleteFrom(T_RSS)
			.where(T_RSS.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		sql.execute();
	}

	public void update(RssBean bean) throws DataAccessException {
		UpdateConditionStep<TRssRecord> sql = jooq
		// @formatter:off
			.update(T_RSS)
			.set(T_RSS.MSG, bean.getMessage())
			.where(T_RSS.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		sql.execute();
	}
}
