package de.jottyfan.camporganizer.module.rss;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class RssBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public final Integer pk;
	public String recipient;
	public String message;
	public LocalDateTime pubdate;

	public RssBean(Integer pk) {
		this.pk = pk;
	}

	public String getMessage80() {
		return message == null ? null : (message.length() > 80 ? message.substring(0, 80).concat("...") : message);
	}

	public Integer getPk() {
		return pk;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getPubdate() {
		return pubdate;
	}

	public void setPubdate(LocalDateTime pubdate) {
		this.pubdate = pubdate;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
}
