package de.jottyfan.camporganizer.module.rss;

import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.jottyfan.camporganizer.module.camplist.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class RssController extends CommonController {

	@Autowired
	private RssService service;

	@GetMapping("/rss/{recipientCode}")
	public void toRssDirectly(@PathVariable String recipientCode, HttpServletResponse response) {
		service.getRss(recipientCode, response);
	}
}
