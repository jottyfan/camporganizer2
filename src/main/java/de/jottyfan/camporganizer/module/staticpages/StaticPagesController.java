package de.jottyfan.camporganizer.module.staticpages;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.jottyfan.camporganizer.module.camplist.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class StaticPagesController extends CommonController {

	/**
	 * load the index page
	 *
	 * @return the index page
	 */
	@GetMapping("/")
	public String getIndex() {
		return "/index";
	}

	/**
	 * load the sub page
	 *
	 * @return the sub page
	 */
	@GetMapping("/reports/{subpage}")
	public String getSubpage(@PathVariable("subpage") String subpage) {
		return "/reports/" + subpage;
	}

	/**
	 * load the verein page
	 *
	 * @return the verein page
	 */
	@GetMapping("/verein")
	public String getVerein() {
		return "/verein";
	}

	/**
	 * load the kontakt page
	 *
	 * @return the kontakt page
	 */
	@GetMapping("/kontakt")
	public String getKontakt() {
		return "/kontakt";
	}

	/**
	 * load the allgemeines page
	 *
	 * @return the allgemeines page
	 */
	@GetMapping("/allgemeines")
	public String getAllgemeines() {
		return "/allgemeines";
	}

	/**
	 * load the datenschutz page
	 *
	 * @return the datenschutz page
	 */
	@GetMapping("/datenschutz")
	public String getDatenschutz() {
		return "/datenschutz";
	}

	/**
	 * load the impressum page
	 *
	 * @return the impresum page
	 */
	@GetMapping("/impressum")
	public String getImpressum() {
		return "/impressum";
	}

}
