package de.jottyfan.camporganizer.db;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;

/**
 *
 * @author jotty
 *
 */
public class EnumConverter {

	/**
	 * get the German names of the camprole
	 *
	 * @param role the role
	 * @return the German names
	 */
	public static final String role2GermanNames(EnumCamprole role) {
		if (EnumCamprole.student.equals(role)) {
			return "Teilnehmer";
		} else if (EnumCamprole.director.equals(role)) {
			return "Leitungsteam";
		} else if (EnumCamprole.feeder.equals(role)) {
			return "Küchenteam";
		} else if (EnumCamprole.teacher.equals(role)) {
			return "Mitarbeiter";
		} else if (EnumCamprole.observer.equals(role)) {
			return "Mitarbeiterkind";
		} else {
			return role == null ? null : role.getLiteral();
		}
	}

	/**
	 * get the German names of the sex
	 *
	 * @param sex the sex
	 * @return the German names
	 */
	public static final String sex2GermanNames(EnumSex sex) {
		if (EnumSex.female.equals(sex)) {
			return "weiblich";
		} else if (EnumSex.male.equals(sex)) {
			return "männlich";
		} else {
			return sex == null ? null : sex.getLiteral();
		}
	}

	/**
	 * get all the enum values in a list of German translations
	 *
	 * @return the list
	 */
	public static final List<KeyValueBean> getSexes() {
		List<KeyValueBean> list = new ArrayList<>();
		list.add(new KeyValueBean().of(EnumSex.female.getLiteral(), "weiblich"));
		list.add(new KeyValueBean().of(EnumSex.male.getLiteral(), "männlich"));
		return list;
	}

	/**
	 * get all the enum values in a list of German translations
	 *
	 * @return the list
	 */
	public static final List<KeyValueBean> getRoles() {
		List<KeyValueBean> list = new ArrayList<>();
		list.add(new KeyValueBean().of(EnumCamprole.student.getLiteral(), "Teilnehmer"));
		list.add(new KeyValueBean().of(EnumCamprole.teacher.getLiteral(), "Mitarbeiter"));
		list.add(new KeyValueBean().of(EnumCamprole.director.getLiteral(), "Leitungsteam"));
		list.add(new KeyValueBean().of(EnumCamprole.feeder.getLiteral(), "Küchenteam"));
		list.add(new KeyValueBean().of(EnumCamprole.observer.getLiteral(), "Mitarbeiterkind"));
		return list;
	}
}
