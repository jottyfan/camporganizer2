package de.jottyfan.camporganizer.db;

import java.io.Serializable;

/**
 *
 * @author jotty
 *
 */
public class KeyValueBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String key;
	private String value;

	/**
	 * create a new bean from the arguments
	 *
	 * @param key   the key
	 * @param value the value
	 * @return the bean
	 */
	public KeyValueBean of(String key, String value) {
		KeyValueBean bean = new KeyValueBean();
		bean.setKey(key);
		bean.setValue(value);
		return bean;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
