package de.jottyfan.camporganizer;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jotty
 *
 */
@SpringBootApplication
@RestController
public class Main extends SpringBootServletInitializer {

	private static final Logger LOGGER = LogManager.getLogger(Main.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Main.class);
	}

	public static void main(String[] args) {
		Path path = Paths.get("");
		String p = path.toAbsolutePath().toString();
		p = p.substring(p.lastIndexOf("/") + 1);
		LOGGER.info("running in {}", p);
		// TODO: put p + "properties" somehow into consideration to load the application.properties
		SpringApplication.run(Main.class, args);
	}
}
