class MyToggle {
	constructor() {
	}

	toggle(divid) {
		$("[id='" + divid + "']").toggle();
	}
}

filterAllOfClass = function(filterselector, haystackselector) {
  $(haystackselector).each(function() {
    var t = $($(this).children()[0]).text();
    var filter = $(filterselector).val();
    if (filter == '' || filter == '*') {
	    $(this).show();
    } else if (t.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
	    $(this).show();
    } else {
	    $(this).hide();
    }
  });
}
