progress = {
	start : function(zin) {
	  zin = zin || 1000;
	  $("body").append("<div id=\"requestopen\" class=\"loading\" style=\"z-index: " + zin + " !important\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
	},
	stop : function() {
		$("#requestopen").remove();
	}
}