package de.jottyfan.camporganizer.module.registration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;

/**
 *
 * @author jotty
 *
 */
public class TestKeycloakRepository {

	/**
	 * test registration
	 */
	@Test
	public void testRegister() {
		KeycloakRepository repository = new KeycloakRepository();
		UserRepresentation user = repository.getUserRepresentation("Hans", "Dampf", "hans", "password", "hans@dampf.org");
		UsersResource resource = repository.getUsersResource("http://localhost:8080", "ow", "owadmin", "password", "biblecamp");
		assertTrue(repository.register(resource, user));
	}
}
